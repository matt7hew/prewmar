const footerYear = document.querySelector('.footer__year');
const hamburger = document.querySelector('.hamburger');
const navMobile = document.querySelector('.nav-mobile');
const allNavItems = document.querySelectorAll('.nav-mobile__item');

const showNavMobile = (item) => {
	navMobile.classList.toggle('active');
	hamburger.classList.toggle('is-active');

	allNavItems.forEach((item) => {
		item.addEventListener('click', () => {
			navMobile.classList.remove('active');
			hamburger.classList.remove('is-active');
		});
	});
};

const handleCurrentYear = () => {
	const year = new Date().getFullYear();
	footerYear.innerText = year;
};

hamburger.addEventListener('click', showNavMobile);
handleCurrentYear();
AOS.init();
